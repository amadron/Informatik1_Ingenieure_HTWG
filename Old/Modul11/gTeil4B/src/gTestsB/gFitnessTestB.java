package gTestsB;

import java.util.Date;

public class gFitnessTestB extends gGenericTestB {

    public gFitnessTestB(String gNameB) {
        super(gNameB, new Date());
        this.gMeasureB = new gMeasurementB[4];
    }

    public String ganalyzeValuesB()
    {
        return "Funktion noch nicht implementiert";
    }

    public void gstartTestB()
    {
        super.gstartTestB();
    }

    public String toString()
    {
        return "Name: " + gNameB + ", Typ: Fitnesstest";
    }
}
