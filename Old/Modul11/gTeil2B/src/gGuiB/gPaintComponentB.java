package gGuiB;

import javax.swing.*;
import java.awt.*;

/**
 * Created by benedict on 30.06.16.
 */
public class gPaintComponentB extends JPanel {

    @Override
    public void paintComponent(Graphics g)
    {
        int gHeightB = getHeight();
        int gWidthB = getWidth();
        int gAxisYPosB = (int) (gHeightB * 0.9);
        int gAxisXPosB = (int) (gWidthB * 0.1);
        g.drawLine(gAxisXPosB, gAxisYPosB, gWidthB, gAxisYPosB);
        g.drawLine(gAxisXPosB, gAxisYPosB, gAxisXPosB, 0);

        g.drawString("Puls", 0, 10);
        g.drawString("Blutdruck", 0, 20);
        g.drawString("Messwert", gWidthB - 100, gAxisYPosB + (int) (gHeightB * 0.05));

    }
}
