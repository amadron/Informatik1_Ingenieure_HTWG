import java.text.SimpleDateFormat;
import java.util.Date;

public class gGenericTestB {
    String gNameB;
    Date gTimeB;

    public gGenericTestB(String gNameB, Date gTimeB){
        gSetNameB(gNameB);
        gSetTimeB(gTimeB);
    }

    public String gGetNameB() {
        return gNameB;
    }

    public void gSetNameB(String gNameB) {
        this.gNameB = gNameB;
    }

    public void gSetTimeB(Date gTimeB){
        this.gTimeB = gTimeB;
    }

    public Date gGetTimeB() {
        return gTimeB;
    }

    public void gprintValuesB()
    {
        String gReturnB = "Generict Test Name: " + gNameB + ", Zeitpunkt: " + gTimeB;
        System.out.println(gReturnB);
    }
}
