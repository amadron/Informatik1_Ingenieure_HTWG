import java.util.Date;

public class gTestAppB {

    public static void main(String args[])
    {
        gGenericTestB gGenTestB1 = new gGenericTestB("Test1", new Date());
        gGenTestB1.gprintValuesB();
        gGenericTestB gGenTestB2 = new gGenericTestB("Test2", new Date());
        gGenTestB2.gprintValuesB();
        gGenericTestB gGenTestB3 = new gGenericTestB("Test3", new Date());
        gGenTestB3.gprintValuesB();
        gMeasurementB gMeasureB1 = new gMeasurementB(100, 120, 130, new Date());
        gMeasureB1.gPrintWerteB();
        gMeasurementB gMeasureB2 = new gMeasurementB(90, 100, 120, new Date());
        gMeasureB2.gPrintWerteB();
        gMeasurementB gMeasureB3 = new gMeasurementB(80, 110, 111, new Date());
    }
}
