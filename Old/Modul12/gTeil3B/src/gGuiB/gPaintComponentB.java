package gGuiB;

import gTestsB.gGenericTestB;
import gTestsB.gMeasurementB;

import javax.swing.*;
import java.awt.*;

public class gPaintComponentB extends JPanel {

    gMainContentB main;

    public gPaintComponentB(gMainContentB main)
    {
        this.main = main;
    }

    @Override
    public void paintComponent(Graphics g)
    {
        int gHeightB = getHeight();
        int gWidthB = getWidth();
        int gAxisYPosB = (int) (gHeightB * 0.9);
        int gAxisXPosB = (int) (gWidthB * 0.1);
        g.drawLine(gAxisXPosB, gAxisYPosB, gWidthB, gAxisYPosB);
        g.drawLine(gAxisXPosB, gAxisYPosB, gAxisXPosB, 0);
        g.drawString("Puls", 0, 10);
        g.drawString("Blutdruck", 0, 20);
        g.drawString("Messwert", gWidthB - 100, gAxisYPosB + (int) (gHeightB * 0.05));
        gGenericTestB gTestB = main.ggetActualTest();
        if(gTestB != null) {
            gMeasurementB[] gMeasure = gTestB.gGetMeasurementB();
            if (gMeasure != null) {
                int gDistEdgeB = 10;
                int gXAxisLengthB = gWidthB - gAxisXPosB;
                int gYAxisLengthB = gHeightB -  (gHeightB - gAxisYPosB);
                int gyMinValB = 0;
                int gyMaxValB = 200;
                int gyRangeB = gyMaxValB - gyMinValB;
                float gYStepB = (float)gYAxisLengthB / (float)gyRangeB ;
                //System.out.println("Y Step: " + gYStepB);
                int gDistanceXB = (int) ((gXAxisLengthB - 2 * gDistEdgeB) / gMeasure.length);
                for (int i = 0; i < gMeasure.length; i++) {
                    if (gMeasure[i] != null) {
                        //System.out.println("Test " + i);
                        int gXValB = i * gDistanceXB + gAxisXPosB + gDistEdgeB;
                        int gDiaBlutPosYB = gYAxisLengthB - ((int) ((float)(gMeasure[i].gGetDiaBlutdrB()) * gYStepB));
                        int gPulsPosYB = gYAxisLengthB -  (int)((float)(gMeasure[i].gGetPulsB()) * gYStepB );
                        int gSysBlutPosYB = gYAxisLengthB - ((int) ((float)(gMeasure[i].gGetSysBlutDrB()) *  gYStepB));
                        //System.out.println("DiaBlutPosY: " + gDiaBlutPosYB + " Val: " + gMeasure[i].gGetDiaBlutdrB());
                        g.fillOval(gXValB, gDiaBlutPosYB, 5, 5);
                        //System.out.println("PulsY: " + gDiaBlutPosYB + " Val: " + gMeasure[i].gGetPulsB());
                        g.fillOval(gXValB, gPulsPosYB , 5, 5);
                        //System.out.println("SysBlutY: " + gSysBlutPosYB + " Val: " + gMeasure[i].gGetSysBlutDrB());
                        g.fillOval(gXValB, gSysBlutPosYB, 5, 5);
                    }
                }
            }
        }
    }
}
