package gGuiB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class gTestControlB extends JDialog implements ActionListener{

    protected JLabel gLabelB = new JLabel("Der Test läuft automatisch durch ansonsten Weiter Klicken");
    protected JButton gCancelB = new JButton("Abbrechen");
    protected JButton gContinueB = new JButton("Weiter");

    protected Timer gtheTimerB = new Timer(0, this);
    protected int gStateB = 0;

    public gTestControlB()
    {
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        gLabelB.setPreferredSize(new Dimension(200, 200));
        getContentPane().add(gLabelB);
        JPanel gButtonPanelB = new JPanel();
        getContentPane().add(gButtonPanelB);
        gButtonPanelB.setLayout(new BoxLayout(gButtonPanelB, BoxLayout.X_AXIS));
        gContinueB.addActionListener(this);
        gButtonPanelB.add(gCancelB);
        gCancelB.addActionListener(this);
        gButtonPanelB.add(gContinueB);
        getContentPane().add(gButtonPanelB);
        this.pack();
        this.setVisible(true);
    }

    protected void gstateMachineB()
    {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object gSourceB = e.getSource();
        if(gSourceB == gContinueB || gSourceB == gtheTimerB){
            gstateMachineB();
        }
        if(gSourceB == gCancelB) {
            gtheTimerB.stop();
            this.dispose();
        }
    }
}
