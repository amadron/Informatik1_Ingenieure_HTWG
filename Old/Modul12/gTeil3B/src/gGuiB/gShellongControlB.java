package gGuiB;

public class gShellongControlB extends gTestControlB {

    public gShellongControlB()
    {
        super();
        gtheTimerB.setInitialDelay(5000);
        gtheTimerB.start();
    }

    protected void gstateMachineB()
    {
        if(gtheTimerB.isRunning()) {
            gtheTimerB.stop();
        }
        String gTextB = null;
        switch (gStateB)
        {
            case 0:
                gTextB = "Legen Sie sich hin!";
                break;
            case 1:
                gTextB = "Messen Sie nun";
                break;
            case 2:
                gTextB = "Bleiben Sie liegen!";
                break;
            case 3:
                gTextB = "Messen Sie nun";
                break;
            case 4:
                gTextB = "Bleiben Sie liegen!";
                break;
            case 5:
                gTextB = "Messen Sie nun";
                break;
            case 6:
                gTextB = "Stehen Sie auf!";
                break;
            case 7:
                gTextB = "Messen Sie nun";
                break;
            case 8:
                gTextB = "Bleiben Sie stehen!";
                break;
            case 9:
                gTextB = "Messen Sie nun";
                break;
            case 10:
                gTextB = "Bleiben Sie stehen!";
                break;
            case 11:
                gTextB = "Messen Sie nun ein letztes mal";
                break;
            case 12:
                gtheTimerB.stop();
                dispose();
                break;
            default:
                break;
        }
        if(gTextB != null) {
            gLabelB.setText(gTextB);
            gStateB++;
            gtheTimerB.start();
        }
    }

}
