package gGuiB;

import gTestsB.gGenericTestB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class gEditNameB extends JDialog implements ActionListener {

    private JTextField gNameB;
    private JLabel gNameTextB = new JLabel("Name:");
    private JButton gConfirmB = new JButton("Ok");
    private gGenericTestB gTestB;

    public gEditNameB(gMainContentB gParentB, gGenericTestB gTestB){

        super((JFrame) SwingUtilities.getWindowAncestor(gParentB), "Test Name Bearbeiten", true);
        this.gTestB = gTestB;
        this.setModal(true);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        JPanel gNamePanelB = new JPanel();
        gNamePanelB.setLayout(new BoxLayout(gNamePanelB, BoxLayout.X_AXIS));
        gNameB = new JTextField(gTestB.gGetNameB());
        gNamePanelB.add(gNameTextB);
        gNameB.setPreferredSize(new Dimension(150, 30));
        gNamePanelB.add(gNameB);
        getContentPane().add(gNamePanelB);

        gConfirmB.addActionListener(this);
        getContentPane().add(gConfirmB);
        this.pack();
        this.setVisible(true);
    }



    @Override
    public void actionPerformed(ActionEvent e) {
        gTestB.gSetNameB(gNameB.getText());
        dispose();
    }
}
