package gGuiB;

import gTestsB.gFitnessTestB;
import gTestsB.gGenericTestB;
import gTestsB.gSchellongTestB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class gMainContentB extends JPanel implements ActionListener {

    gGenericTestB[] gTestsB = new gGenericTestB[10];
    int gCounterB = 0;
    gGenericTestB gactualTestB;

    private JButton gStartTestButtonB = new JButton("Test starten");
    private JButton gMesswerteLeseButtonB = new JButton("Messwerte einlesen");
    private JButton gAnwendungBeendenB = new JButton("Anwendung beenden");
    private JComboBox<gGenericTestB> gAktuellerTestBoxB = new JComboBox<gGenericTestB>();

    gMainContentB gContentPaneB;

    JMenuItem gStartTestB = new JMenuItem("Test Starten");
    JMenuItem gMesswerteEinlesenB = new JMenuItem("Messwerte einlesen");
    JMenuItem gDatenLadenB = new JMenuItem("Messdaten laden");
    JMenuItem gDatenSpeichernB = new JMenuItem("Daten speichern");
    JMenuItem gBeendenB = new JMenuItem("Beenden");
    JMenuItem gNeuerTestB = new JMenuItem("Neuer Test");
    JMenuItem gEditTestNameB = new JMenuItem("Testname bearbeiten");
    JMenuItem gTestDruckenB = new JMenuItem("Test ausdrucken");
    JLabel gAnalyzeValueB = new JLabel("AnalyzeValues");

    public gGenericTestB ggetActualTest()
    {
        return gactualTestB;
    }

    public gMainContentB()
    {
        this.setLayout(new BorderLayout(10, 10));

        JMenuBar gMenuBarB = new JMenuBar();
        JMenu gMenuB = new JMenu("Datei");

        gMenuBarB.add(gMenuB);


        gStartTestB.addActionListener(this);
        gMenuB.add(gStartTestB);

        gMesswerteEinlesenB.addActionListener(this);
        gMenuB.add(gMesswerteEinlesenB);

        gDatenLadenB.addActionListener(this);
        gMenuB.add(gDatenLadenB);

        gDatenSpeichernB.addActionListener(this);
        gMenuB.add(gDatenSpeichernB);

        gBeendenB.addActionListener(this);
        gMenuB.add(gBeendenB);

        gNeuerTestB.addActionListener(this);
        gMenuB.add(gNeuerTestB);

        gEditTestNameB.addActionListener(this);
        gMenuB.add(gEditTestNameB);

        gTestDruckenB.addActionListener(this);
        gMenuB.add(gTestDruckenB);
        this.add(gMenuBarB, BorderLayout.NORTH);

        JPanel gMenuPanelB = new JPanel();
        gMenuPanelB.setLayout(new BoxLayout(gMenuPanelB, BoxLayout.Y_AXIS));


        gStartTestButtonB.addActionListener(this);
        gMenuPanelB.add(gStartTestButtonB);

        gMesswerteLeseButtonB.addActionListener(this);
        gMenuPanelB.add(gMesswerteLeseButtonB);

        gAnwendungBeendenB.addActionListener(this);
        gMenuPanelB.add(gAnwendungBeendenB);

        gAnalyzeValueB.setPreferredSize(new Dimension(100, 70));
        gMenuPanelB.add(gAnalyzeValueB);

        gAktuellerTestBoxB.addActionListener(this);
        gMenuPanelB.add(gAktuellerTestBoxB);
        this.add(BorderLayout.LINE_START, gMenuPanelB);
        gPaintComponentB gGraphicB = new gPaintComponentB(this);
        this.add(BorderLayout.CENTER, gGraphicB);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object gSourceB = e.getSource();
        if(gSourceB == gMesswerteEinlesenB || gSourceB == gMesswerteLeseButtonB)
        {
            if(gactualTestB != null)
            {
                gactualTestB.gReadValuesB();
                gAnalyzeValueB.setText(ggetActualTest().ganalyzeValuesB());
            }
        }
        if(gSourceB == gDatenLadenB)
        {
            gGenericTestB gTestB = null;
            JFileChooser gfChooserB = new JFileChooser();
            if(gfChooserB.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
                try{
                    FileInputStream gfileInB = new FileInputStream(gfChooserB.getSelectedFile().getAbsolutePath());
                    ObjectInputStream gInB = new ObjectInputStream(gfileInB);
                    gTestB = (gGenericTestB) gInB.readObject();
                    gAktuellerTestBoxB.addItem(gTestB);
                    gactualTestB = gTestB;
                    gInB.close();
                    gfileInB.close();
                }
                catch(Exception gExB)
                {
                    gExB.printStackTrace();
                }
            }

        }
        if(gSourceB == gDatenSpeichernB)
        {
            JFileChooser gfChooserB = new JFileChooser();
            if(gfChooserB.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
            {
                try {
                    FileOutputStream gfileOutB = new FileOutputStream(gfChooserB.getSelectedFile().getAbsolutePath());
                    ObjectOutputStream goutB = new ObjectOutputStream(gfileOutB);
                    goutB.writeObject(ggetActualTest());
                    goutB.close();
                    gfileOutB.close();
                }
                    catch(IOException gExB)
                    {
                        gExB.printStackTrace();
                    }
                {

                }
            }
        }
        if(gSourceB == gBeendenB || gSourceB == gAnwendungBeendenB)
        {
            System.exit(0);
        }
        if(gSourceB == gNeuerTestB)
        {
            if(gCounterB < gTestsB.length)
            {
                gGenericTestB gTmpTestB;
                gNewTestDialogB gNewTDialogB = new gNewTestDialogB(this);
                if(gNewTDialogB.gwasSuccessfullyB()) {
                    String gTypeB = gNewTDialogB.gGetTypeB();
                    String gNameB = gNewTDialogB.gGetNameB();
                    if (gTypeB == "Shellongtest") {
                        gTmpTestB = new gSchellongTestB(gNameB);
                    } else {
                        gTmpTestB = new gFitnessTestB(gNameB);
                    }
                    gactualTestB = gTmpTestB;
                    gAktuellerTestBoxB.addItem(gTmpTestB);
                    gAktuellerTestBoxB.setSelectedItem(gTmpTestB);
                    gTestsB[gCounterB] = gTmpTestB;
                    gCounterB++;
                }
            }
        }
        if(gSourceB == gEditTestNameB)
        {
            if(gactualTestB != null){
                gEditNameB gEditB = new gEditNameB(this, gactualTestB);
            }
        }
        if(gSourceB == gTestDruckenB) {
            if (gactualTestB != null) {
                try {
                    JFileChooser gChoosB = new JFileChooser();
                    if (gChoosB.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                        PrintWriter gthePrintWriterB = new PrintWriter(gChoosB.getSelectedFile());
                        gactualTestB.gPrintValuesB(gthePrintWriterB);
                        gthePrintWriterB.close();
                    }
                } catch (Exception gExB) {
                    gExB.printStackTrace();
                }

            }
        }
        if(gSourceB == gStartTestButtonB || gSourceB == gStartTestB)
        {
            if(gactualTestB != null) {
                gactualTestB.gstartTestB();
                gAnalyzeValueB.setText(gactualTestB.ganalyzeValuesB());
            }
        }
        if(gSourceB == gAktuellerTestBoxB)
        {
            if(gAktuellerTestBoxB.getItemCount() > 0)
            {
                gactualTestB = (gGenericTestB) gAktuellerTestBoxB.getSelectedItem();
            }
        }
        repaint();
    }
}
