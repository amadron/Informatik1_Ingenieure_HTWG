package gTestsB;

import gGuiB.gFitnessControlB;

import java.io.Serializable;
import java.util.Date;

public class gFitnessTestB extends gGenericTestB implements Serializable {

    public gFitnessTestB(String gNameB) {
        super(gNameB, new Date());
        this.gMeasureB = new gMeasurementB[4];
    }

    public String ganalyzeValuesB()
    {
        return "Funktion noch nicht implementiert";
    }

    public void gstartTestB()
    {
        gFitnessControlB gTestDB = new gFitnessControlB();
        System.out.println("Start test");
    }

    public String toString()
    {
        return "Name: " + gNameB + ", Typ: Fitnesstest";
    }
}
