package gGuiB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class gNewTestDialogB extends JDialog implements ActionListener{

    private JLabel gTestNameDisplayB = new JLabel("Test name:");
    private JTextField gTestNameB = new JTextField();
    private JComboBox<String> gTestTypeBoxB = new JComboBox<>();
    private JButton gCancelButtonB = new JButton("Cancel");
    private JButton gConfirmButtonB = new JButton("Ok");
    private boolean gPressedOkB = false;
    public gNewTestDialogB(gMainContentB gParentB)
    {
        super((JFrame) SwingUtilities.getWindowAncestor(gParentB), "Neuer Test", true);
        this.setModal(true);
        JPanel gNamePanelB = new JPanel();
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        gTestNameB.setPreferredSize(new Dimension(150, 30));
        gNamePanelB.add(gTestNameDisplayB);
        gNamePanelB.add(gTestNameB);
        gTestNameB.addActionListener(this);

        getContentPane().add(gNamePanelB);
        gTestTypeBoxB.addItem("Fitnesstest");
        gTestTypeBoxB.addItem("Shellongtest");

        getContentPane().add(gTestTypeBoxB);
        JPanel gButtonPanelB = new JPanel();
        gButtonPanelB.setLayout(new BoxLayout(gButtonPanelB, BoxLayout.X_AXIS));
        gConfirmButtonB.addActionListener(this);
        gCancelButtonB.addActionListener(this);
        gButtonPanelB.add(gConfirmButtonB);
        gButtonPanelB.add(gCancelButtonB);
        getContentPane().add(gButtonPanelB);
        this.pack();
        this.setVisible(true);
    }

    public String gGetTypeB(){
        return (String) gTestTypeBoxB.getSelectedItem();
    }

    public String gGetNameB(){
        return gTestNameB.getText();
    }

    public boolean gwasSuccessfullyB()
    {
        return gPressedOkB;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object gSourceB = e.getSource();
        if(gSourceB == gConfirmButtonB)
        {
            gPressedOkB = true;
            dispose();
        }
        if(gSourceB == gCancelButtonB)
        {
            dispose();
        }
    }
}
