package gGuiB;

import gTestsB.gGenericTestB;

import javax.swing.*;
import java.awt.*;

public class gHauptFensterB extends JFrame {

    public gHauptFensterB() {
        gMainContentB gContentPaneB = new gMainContentB();
        this.setContentPane(gContentPaneB);
        this.setMinimumSize(new Dimension(640, 480));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

}
