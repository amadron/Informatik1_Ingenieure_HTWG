package gTestsB;

import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Date;

public class gMeasurementB implements Serializable{
    double gPulsB;
    double gSysBlutdrB;
    double gDiaBlutdrB;
    Date gTimeB;

    public gMeasurementB(double gPulsB, double gSysBlutDrB, double gDiaBlutdrB,Date gTimeB)
    {
        this.gPulsB = gPulsB;
        this.gSysBlutdrB = gSysBlutDrB;
        this.gDiaBlutdrB = gDiaBlutdrB;
        this.gTimeB = gTimeB;
    }

    public double gGetPulsB()
    {
        return gPulsB;
    }

    public double gGetSysBlutDrB()
    {
        return gSysBlutdrB;
    }

    public double gGetDiaBlutdrB()
    {
        return gDiaBlutdrB;
    }

    public Date gGetTimeB()
    {
        return gTimeB;
    }

    public void gprintValuesB(PrintWriter gPrinterB)
    {
        String gStrB = "Puls: " + gPulsB + "\nSystolischer Blutdruck: " + gSysBlutdrB + "\nDiastolischer Blutdruck: "
                + gDiaBlutdrB + "\nZeitpunkt: " + gTimeB.toString();
        if(gPrinterB != null)
            gPrinterB.println(gStrB);
    }
}
