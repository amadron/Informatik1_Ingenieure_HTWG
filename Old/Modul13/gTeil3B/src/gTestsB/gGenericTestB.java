package gTestsB;
import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class gGenericTestB implements Serializable{
    String gNameB;
    Date gTimeB;
    gMeasurementB gMeasureB[];

    public gGenericTestB(String gNameB, Date gTimeB){
        gSetNameB(gNameB);
        gSetTimeB(gTimeB);
    }

    public String gGetNameB() {
        return gNameB;
    }

    public void gSetNameB(String gNameB) {
        this.gNameB = gNameB;
    }

    public void gSetTimeB(Date gTimeB){
        this.gTimeB = gTimeB;
    }

    public Date gGetTimeB() {
        return gTimeB;
    }

    public gMeasurementB[] gGetMeasurementB()
    {
        return gMeasureB;
    }

    public void gReadValuesB()
    {
        JFileChooser gChooserB = new JFileChooser();
        if(gChooserB.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
        {
            try{
                BufferedReader gBrB = new BufferedReader(new FileReader(gChooserB.getSelectedFile()));
                String glineB;
                int counter = 0;
                while((glineB = gBrB.readLine()) != null)
                {
                    counter++;
                }
                gMeasureB = new gMeasurementB[counter];
                gBrB.close();
                gBrB = new BufferedReader(new FileReader(gChooserB.getSelectedFile()));
                counter = 0;
                while((glineB = gBrB.readLine()) != null)
                {
                    String[] gValuesB = glineB.split(" ");
                    System.out.println("Line: " + glineB +" Vals:" + gValuesB[0] + " " + gValuesB[1] + " " + gValuesB[2]);
                    if(gValuesB.length != 3){
                        throw new Exception("Wrong amount of values in line " + counter);
                    }
                    int val1 = Integer.parseInt(gValuesB[0]);
                    int val2 = Integer.parseInt(gValuesB[1]);
                    int val3 = Integer.parseInt(gValuesB[2]);
                    System.out.println("Val1 " + val1 + " val2" + val2 + " val3 " + val3);
                    gMeasureB[counter] = new gMeasurementB(val1, val2, val3,new Date());
                    counter++;
                }
                gBrB.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public void gPrintValuesB(PrintWriter gthePrinterWriterB) {
        String gPrintB = "Generic Test Name: " + gNameB + "\nZeitpunkt: " + gTimeB;
        if (gthePrinterWriterB != null)
            gthePrinterWriterB.println(gPrintB);
        for (int i = 0; i < gMeasureB.length; i++) {
            gMeasureB[i].gprintValuesB(gthePrinterWriterB);
        }
        if (gthePrinterWriterB != null)
            gthePrinterWriterB.println(this.ganalyzeValuesB());
    }

    private int ggetRandomB(int gMinB, int gMaxB)
    {
        int gRetB = 0;
        int gRangeB = gMaxB - gMinB;
        gRetB = (int) (Math.random() * gRangeB) + gMinB;
        return gRetB;
    }

    public void gstartTestB()
    {
        this.gTimeB = new Date();
    }

    public abstract String ganalyzeValuesB();
}
