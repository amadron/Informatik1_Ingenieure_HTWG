package gGuiB;

import javax.swing.*;
import java.awt.*;

public class gMainContentB extends JPanel {

    public gMainContentB()
    {
        JPanel gMenuPanelB = new JPanel();
        gMenuPanelB.setLayout(new BoxLayout(gMenuPanelB, BoxLayout.Y_AXIS));
        this.setLayout(new BorderLayout(10, 10));
        JButton gStartTestButtonB = new JButton("Test starten");
        gMenuPanelB.add(gStartTestButtonB);
        JButton gMesswerteLeseButtonB = new JButton("Messwerte einlesen");
        gMenuPanelB.add(gMesswerteLeseButtonB);
        JButton gAnwendungBeendenB = new JButton("Anwendung beenden");
        gMenuPanelB.add(gAnwendungBeendenB);
        JComboBox<String> gAktuellerTestBoxB = new JComboBox<String>();

        gMenuPanelB.add(gAktuellerTestBoxB);
        this.add(BorderLayout.LINE_START, gMenuPanelB);
    }
}
