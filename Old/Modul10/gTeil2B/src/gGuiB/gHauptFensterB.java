package gGuiB;

import javax.swing.*;
import java.awt.*;

public class gHauptFensterB extends JFrame {

    gMainContentB gContentPaneB;

    public gHauptFensterB()
    {
        gContentPaneB  = new gMainContentB();
        this.setContentPane(gContentPaneB);
        this.setMinimumSize(new Dimension(640, 480));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JMenuBar gMenuBarB = new JMenuBar();
        JMenu gMenuB = new JMenu("Datei");
        gMenuBarB.add(gMenuB);
        JMenuItem gStartTestB = new JMenuItem("Test Starten");
        gMenuB.add(gStartTestB);
        JMenuItem gMesswerteEinlesenB = new JMenuItem("Messwerte einlesen");
        gMenuB.add(gMesswerteEinlesenB);
        JMenuItem gDatenLadenB = new JMenuItem("Messdaten laden");
        gMenuB.add(gDatenLadenB);
        JMenuItem gDatenSpeichernB = new JMenuItem("Daten speichern");
        gMenuB.add(gDatenSpeichernB);
        JMenuItem gBeendenB = new JMenuItem("Beenden");
        gMenuB.add(gBeendenB);
        JMenuItem gNeuerTestB = new JMenuItem("Neuer Test");
        gMenuB.add(gNeuerTestB);
        JMenuItem gEditTestNameB = new JMenuItem("Testname bearbeiten");
        gMenuB.add(gEditTestNameB);
        JMenuItem gTestDruckenB = new JMenuItem("Test ausdrucken");
        gMenuB.add(gTestDruckenB);

        this.setJMenuBar(gMenuBarB);
        this.pack();
        this.setVisible(true);
    }
}
