package gTestsB;
import java.text.SimpleDateFormat;
import java.util.Date;

public class gGenericTestB {
    String gNameB;
    Date gTimeB;
    gMeasurementB gMeasureB[];

    public gGenericTestB(String gNameB, Date gTimeB){
        gSetNameB(gNameB);
        gSetTimeB(gTimeB);
    }

    public String gGetNameB() {
        return gNameB;
    }

    public void gSetNameB(String gNameB) {
        this.gNameB = gNameB;
    }

    public void gSetTimeB(Date gTimeB){
        this.gTimeB = gTimeB;
    }

    public Date gGetTimeB() {
        return gTimeB;
    }

    public gMeasurementB[] gGetMeasurement()
    {
        return gMeasureB;
    }

    public void gReadValuesB()
    {
        for(int i = 0; i < gMeasureB.length; i++)
        {
            gMeasureB[i] = new gMeasurementB(Math.random(), Math.random(), Math.random(), new Date());
        }
    }

    public void gPrintValuesB()
    {
        String gPrintB = "Generic Test Name: " + gNameB + "\nZeitpunkt: " + gTimeB;
        System.out.println(gPrintB);
        for(int i = 0; i < gMeasureB.length; i++) {
            gMeasureB[i].gprintValuesB();
        }
    }
}
