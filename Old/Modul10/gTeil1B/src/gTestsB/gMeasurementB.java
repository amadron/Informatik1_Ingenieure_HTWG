import java.util.Date;

public class gMeasurementB {
    int gPulsB;
    int gSysBlutdrB;
    int gDiaBlutdrB;
    Date gTimeB;

    public gMeasurementB(int gPulsB, int gSysBlutDrB, int gDiaBlutdrB,Date gTimeB)
    {
        this.gPulsB = gPulsB;
        this.gSysBlutdrB = gSysBlutDrB;
        this.gDiaBlutdrB = gDiaBlutdrB;
        this.gTimeB = gTimeB;
    }

    public int gGetPulsB()
    {
        return gPulsB;
    }

    public int gGetSysBlutDrB()
    {
        return gSysBlutdrB;
    }

    public int gGetDiaBlutdrB()
    {
        return gDiaBlutdrB;
    }

    public Date gGetTimeB()
    {
        return gTimeB;
    }

    public void gPrintWerteB()
    {
        System.out.println("Puls: " + gPulsB + "\nSystolischer Blutdruck: " + gSysBlutdrB + "\nDiastolischer Blutdruck: "
                + gDiaBlutdrB + "\nZeitpunkt: " + gTimeB.toString());
    }
}
