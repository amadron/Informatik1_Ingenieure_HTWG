package gTestsB;

import gGuiB.gShellongControlB;

import java.io.Serializable;
import java.util.Date;

public class gSchellongTestB extends gGenericTestB implements Serializable{

    public gSchellongTestB(String gNameB) {
        super(gNameB, new Date());
        this.gMeasureB = new gMeasurementB[6];
    }

    public String ganalyzeValuesB()
    {
        double gPulsB = 0, gsysB = 0, gdiasB = 0;
        String gresultB;

        if (gMeasureB[0] != null && gMeasureB[1] != null && gMeasureB[2] != null && gMeasureB[3] != null)
        {
            gPulsB = gMeasureB[3].gGetPulsB() - gMeasureB[2].gGetPulsB();
            gsysB = gMeasureB[3].gGetSysBlutDrB() - gMeasureB[2].gGetSysBlutDrB();
            gdiasB = gMeasureB[3].gGetDiaBlutdrB() - gMeasureB[2].gGetSysBlutDrB();

            if (10 <= gPulsB && gPulsB <= 20 && -8 >= gsysB && gsysB >= -15 && 8 <= gdiasB && gdiasB <= 15)
            {
                gresultB = "Sie sind Gesund";
            }
            else
            {
                gresultB = "Sie sind Krank";
            }
        }
        else
        {
            gresultB = "Schellong Test";
        }
        return gresultB;
    }

    public void gstartTestB()
    {
        gShellongControlB gTestDB = new gShellongControlB();
    }

    public String toString(){
        return "Name: " + gNameB + ",  Typ: Shellongtest";
    }
}
