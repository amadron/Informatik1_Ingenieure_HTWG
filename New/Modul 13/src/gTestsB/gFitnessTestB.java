package gTestsB;

import gGuiB.gFitnessControlB;

import java.io.Serializable;
import java.util.Date;

public class gFitnessTestB extends gGenericTestB implements Serializable {

    public gFitnessTestB(String gNameB) {
        super(gNameB, new Date());
        this.gMeasureB = new gMeasurementB[4];
    }

    public String ganalyzeValuesB()
    {
        double gPulsB = 0, gPuls1B = 0;
        String gresultB;

        if (gMeasureB[0] != null && gMeasureB[1] != null && gMeasureB[2] != null)
        {
            gPulsB = gMeasureB[1].gGetPulsB() - gMeasureB[0].gGetPulsB();
            gPuls1B = gMeasureB[1].gGetPulsB() - gMeasureB[2].gGetPulsB();

            if (gPulsB > 80 || gPuls1B>40)
            {
                gresultB = "Sie sind Fit, weiter so" + "\n";
            }
            else
            {
                gresultB = "Sie sind nicht Fit, treiben Sie mehr sport" + "\n";
            }
        }
        else
        {
            gresultB = "Fitness Test";
        }
        return gresultB;
    }

    public void gstartTestB()
    {
        gFitnessControlB gTestDB = new gFitnessControlB();
        System.out.println("Start test");
    }

    public String toString()
    {
        return "Name: " + gNameB + ", Typ: Fitnesstest";
    }
}
