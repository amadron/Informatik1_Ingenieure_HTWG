package gTestsB;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class gGenericTestB {
    String gNameB;
    Date gTimeB;
    gMeasurementB gMeasureB[];

    public gGenericTestB(String gNameB, Date gTimeB){
        gSetNameB(gNameB);
        gSetTimeB(gTimeB);
    }

    public String gGetNameB() {
        return gNameB;
    }

    public void gSetNameB(String gNameB) {
        this.gNameB = gNameB;
    }

    public void gSetTimeB(Date gTimeB){
        this.gTimeB = gTimeB;
    }

    public Date gGetTimeB() {
        return gTimeB;
    }

    public gMeasurementB[] gGetMeasurementB()
    {
        return gMeasureB;
    }

    public void gReadValuesB()
    {
        for(int i = 0; i < gMeasureB.length; i++)
        {
            gMeasureB[i] = new gMeasurementB(ggetRandomB(80, 120), ggetRandomB(80, 120), ggetRandomB(80, 120), new Date());
        }
    }

    public void gPrintValuesB()
    {
        String gPrintB = "Generic Test Name: " + gNameB + "\nZeitpunkt: " + gTimeB;
        System.out.println(gPrintB);
        for(int i = 0; i < gMeasureB.length; i++) {
            gMeasureB[i].gprintValuesB();
        }
        System.out.println(this.ganalyzeValuesB());
    }

    private int ggetRandomB(int gMinB, int gMaxB)
    {
        int gRetB = 0;
        int gRangeB = gMaxB - gMinB;
        gRetB = (int) (Math.random() * gRangeB) + gMinB;
        return gRetB;
    }

    public void gstartTestB()
    {
        this.gTimeB = new Date();
    }

    public abstract String ganalyzeValuesB();
}
