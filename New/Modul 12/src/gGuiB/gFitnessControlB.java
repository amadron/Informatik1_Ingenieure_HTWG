package gGuiB;

public class gFitnessControlB extends gTestControlB {

    public gFitnessControlB()
    {
        super();
        gtheTimerB.setInitialDelay(5000);
        gtheTimerB.start();
    }

    protected void gstateMachineB() {
        if (gtheTimerB.isRunning()) {
            gtheTimerB.stop();
        }

        String gTextB = null;
        switch (gStateB) {
            case 0:
                gTextB = "Messen Sie erst einmal";
                break;
            case 1:
                gTextB = "Rennen Sie nun den Gang hoch und runter!";
                break;
            case 2:
                gTextB = "Messen Sie nun";
                break;
            case 3:
                gTextB = "Springen Sie 10 mal auf der Stelle!";
                break;
            case 4:
                gTextB = "Rennen Sie ein Stockwerk runtern und wieder hoch";
                break;
            case 5:
                gTextB = "Messen Sie nun";
                break;
            case 6:
                gTextB = "Messen Sie nun ein letztes mal";
                break;
            case 7:
                gtheTimerB.stop();
                dispose();
                break;
            default:
                break;
        }

        if (gTextB != null) {
            gLabelB.setText("<html>"+gTextB+"</html>");
            gStateB++;
            gtheTimerB.start();
        }
    }
}
